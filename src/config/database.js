module.exports = {
    dialect: 'mysql',
    host:'localhost',
    database:'gobarber',
    username: 'root',
    port: 3306,
    password: 'root',
    define: {
        timestamps: true,
        underscored: true,
        underscoredAll: true,
    }
};


