import * as yup from 'yup';
import { startOfHour, parseISO, isBefore } from 'date-fns';
import User from '../models/User';
import File from '../models/File';
import Appointment from '../models/Appointments';

class AppointmentController{

    async index(req, res){
        const appointment = await Appointment.findAll({
            where: { user_id : req.userId, canceled_at: null },
            order: ['date'],
            attributes: [ 'id', 'date' ],
            include: [
                {
                    model: User,
                    as: 'provider',
                    attributes: [ 'id', 'name' ],
                    include: [
                        {
                            model: File,
                            as: 'avatar',
                            attributes: ['id', 'path', 'url']
                        }
                    ]
                }
            ]
        });

        return res.json(appointment);
    }

    async store(req, res){

        const schema = yup.object().shape({
            provider_id: yup.number().required(),
            date: yup.date().required(),
        });

        if(!(await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Erro ao validar os dados' });
        }

        const { provider_id, date } = req.body;

        const isProvider = await User.findOne({
            where: { id: provider_id, provider: true },
        })

        if(!isProvider){
            return res.status(401).json({ error: "você não pode registrar como provedor de serviço" });
        }

        //verifica data se é validator.
        const hourStart = startOfHour(parseISO(date)); 

        // if (hourStart == date){
        //     return res.status(400).json({ hourStart, date });
        // }


        if(isBefore( hourStart, new Date())){
            return res.status(400).json({ error: 'Hora não permitida.' });
        };

        //verifica provedor de servico
        const checkAvailability = await Appointment.findOne({
            where: {
                provider_id,
                canceled_at: null,
                date: hourStart
            },
        });

        if (checkAvailability){
            return res.status(400).json({ error: 'Data não disponível para agendamento'});
        };

        console.log('=========');
        console.log(date, hourStart);
        console.log('=========');

        const appointment = await Appointment.create({
            user_id: req.userId,
            provider_id,
            date,
        });

        return res.json(appointment);
    }
}

export default new AppointmentController();