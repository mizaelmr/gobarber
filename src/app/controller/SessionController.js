import jwt from 'jsonwebtoken';
import authConfig from '../../config/auth';
import * as yup from 'yup';
import User from '../models/User';


class SessionController{
    async store(req, res){

        //validação dos campos
        const schema = yup.object().shape({
            email: yup.string()
                    .email()
                    .required(),
            password: yup.string().required(),
        });

        if (!(await schema.isValid(req.body))) {
            return res.status(400).json({ error: "erro nas validações kkkk" });
        }

        const { email, password} = req.body;
        //verifica se o email existe
        const user = await User.findOne({ where: { email } });

        if(!user){
            return res.status(401).json({ error: "Usuário não encontrado"});
        }

        if (!(await user.checkPassword(password))){
            return res.status(401).json({ error: "A senha não é válida"});
        }

        const { id, name } = user; 

        return res.json({
            user: {
                id,
                name,
                email,
            },
            token: jwt.sign({ id }, authConfig.secret,  {
                expiresIn: authConfig.expiresIn,
            }),
        });
    }
}

export default new SessionController();