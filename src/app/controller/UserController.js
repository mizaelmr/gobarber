import * as yup from 'yup';
import User from '../models/User';

class UserController{

    async index(req, res){
        const users = await User.findAll();
        return res.json(users);
    }

    async store(req, res){

        console.log(req.body);
        //validação dos campos
        const schema = yup.object().shape({
            name: yup.string().required(),
            email: yup.string().email().required(),
            password: yup.string().required().min(6),
        });

        if (!(await schema.isValid(req.body))) {
            return res.status(400).json({ error: "erro nas validações 1" });
        }

        const userExist = await User.findOne({ where: { email: req.body.email} });
        
        if (userExist){
            return res.status(400.).json({ error:  "usuário já existe manolo"  });
        }

        const {id, name, email} = await User.create(req.body);
        return res.json({ id, name, email });    
    }

    async update(req, res){

        const schema = yup.object().shape({
            name: yup.string(),
            email: yup.string().email(),
            oldPassword: yup.string().min(6),
            password: yup.string()
                .min(6)
                .when('oldPassword', (oldPassword, field) => 
                    oldPassword ? field.required() : field  
                ),
            confirmPassword: yup.string().when('password', (password, field) =>
                password ? field.required().oneOf([yup.ref('password')]) : field
            ),
        });

        if (!(await schema.isValid(req.body))) {
            return res.status(400).json({ error: "erro nas validações" });
        }

        const { email, oldPassword } = req.body;
        const user = await User.findOne({ where: { id: req.userId }});
        console.log(req);
        if(email != user.email){
            const userExist = await User.findOne({ where: { email } });

            if (userExist) {
                return res.status(400.).json({ error: "usuário já existe manolooo" });
            }
        }
        // 
        if(oldPassword && !(await user.checkPassword(oldPassword))) {
            return res.status(401).json({ error: "A senha não coincide" });
        }

        const { id, name } = await user.update(req.body);

        return res.json({ id, name, email });
    }

}

export default new UserController();