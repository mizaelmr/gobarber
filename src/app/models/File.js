import Sequelize, { Model } from 'sequelize';

class File extends Model {
    static init(sequelize) {
        super.init({
            name: Sequelize.STRING,
            path: Sequelize.STRING,  
            url: {
                type: Sequelize.VIRTUAL,
                get() {
                    return `http://localhost:7000/files/${this.path}`;
                } 
            },                    
        },
            {
                sequelize,
                //tableName: "Files",
            });

        return this;
    }

}

export default File;