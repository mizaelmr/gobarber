import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';

import UserController from "./app/controller/UserController";
import SessionController from "./app/controller/SessionController";
import FileController from "./app/controller/FileController";
import ProviderController from "./app/controller/ProviderController";
import AppointmenstController from './app/controller/AppointmenstController';

import authMiddleare from './app/middlewares/auth';

const routes = new Router();
const upload = multer(multerConfig);

routes.get('/', UserController.index);
routes.post('/cadastroUsers', UserController.store);
routes.post('/sessions', SessionController.store);

routes.use(authMiddleare);

routes.put('/users', UserController.update);
routes.get('/providers', ProviderController.index);
routes.post('/files', upload.single('file'), FileController.store);
routes.post('/appointments', AppointmenstController.store);
routes.get('/appointments', AppointmenstController.index);

export default routes;